/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class ProjetoTest
{
    
    public ProjetoTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of converterParaDouble method, of class Projeto.
     */
    @Test
    public void testConverterParaDouble() throws Exception
    {
        System.out.println("converterParaDouble");
        String temp = "12/12";
        String do2="1";
        Double d1= Double.parseDouble(do2);
        Double result = AHP.converterParaDouble(temp);
        assertEquals(d1, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of printMat method, of class Projeto.
     */
    @Test
    public void testPrintMat()
    {
        System.out.println("printMat");
        double[][] mat = null;
        AHP.printMat(mat);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of somarColunas method, of class Projeto.
     */
    @Test
    public void testSomarColunas()
    {
        System.out.println("somarColunas");
        double[][] mat = {{1,2,3},{1,2,3},{1,2,3}};
        int linhas = 3;
        int colunas = 3;
        double[] somaColunas = new double[3];
        double [] somacol= {3,6,9};
        AHP.somarColunas(mat, linhas, colunas, somaColunas);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals(somacol, somaColunas, 0);
    }

    /**
     * Test of normalizarMatriz method, of class Projeto.
     */
    @Test
    public void testNormalizarMatriz()
    {
        System.out.println("normalizarMatriz");
        double[][] mat = {{1,1,1},{1,1,1},{1,1,1}};
        int linha = 3;
        int coluna = 3;
        double[][] matNormalizada = new double[3][3];
        double[][] matN = {{(double)1/3,(double)1/3,(double)1/3},{(double)1/3,(double)1/3,(double)1/3},{(double)1/3,(double)1/3,(double)1/3}};
        AHP.normalizarMatriz(mat, linha, coluna, matNormalizada);
        // TODO review the generated test code and remove the default call to fail.
        boolean result=testDoubleEachValue(matN, matNormalizada);
        assertTrue(result);
    }
    public boolean testDoubleEachValue(double [][] matN, double [][] matNorm){
        
        if(matN.length==matNorm.length){
            if(matN[0].length==matNorm[0].length){
                for(int i=0;i<matN.length;i++){
                    for(int j=0;j<matN[0].length;j++){
                        if(matN[i][j]!=matNorm[i][j]){
                            return false;
                        }
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }
    /**
     * Test of criarVetPropAprox method, of class Projeto.
     */
    @Test
    public void testCriarVetPropAprox()
    {
        System.out.println("criarVetPropAprox");
        double[][] matNormalizada = {{1,1,1},{1,1,1},{1,1,1}};
        int linhas = 3;
        int colunas = 3;
        double[] vetProp = new double[3];
        double[] vetTeste = {1,1,1};
        AHP.criarVetPropAprox(matNormalizada, vetProp);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals(vetTeste, vetProp, 0);
    }

    /**
     * Test of valorPropAprox method, of class Projeto.
     */
    @Test
    public void testValorPropAprox()
    {
        System.out.println("valorPropAprox");
        double[][] mat = null;
        int linha = 0;
        int coluna = 0;
        double[] vetProp = new double[3];
        AHP.valorPropAprox(mat, linha, coluna, vetProp);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    
    public boolean testMatPrEachValue(double [][] matPr, double [][] matrizP){
        
        if(matPr.length==matrizP.length){
            if(matPr[0].length==matrizP[0].length){
                for(int i=0;i<matPr.length;i++){
                    for(int j=0;j<matPr[0].length;j++){
                        if(matPr[i][j]!=matrizP[i][j]){
                            return false;
                        }
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }

    

    /**
     * Test of obterValorProprio method, of class Projeto.
     */
    @Test
    public void testObterValorProprio()
    {
        System.out.println("obterValorProprio");
        double[][] matriz = null;
        double expResult = 0.0;
        double result = AHP.obterValorProprio(matriz,matriz.length);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of obterVetorProprio method, of class Projeto.
     */
    @Test
    public void testObterVetorProprio()
    {
        System.out.println("obterVetorProprio");
        double[][] matrizA = null;
        double[][] matrizB = null;
        double valorProprio = 0.0;
        double[] expResult = null;
        double[] result = AHP.obterVetorProprio(matrizA, matrizB, valorProprio);
//        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularConsistencia method, of class Projeto.
     */
    @Test
    public void testCalcularConsistencia()
    {
        System.out.println("calcularConsistencia");
//        Projeto.calcularConsistencia();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularICBiblioteca method, of class Projeto.
     */
    @Test
    public void testCalcularICBiblioteca()
    {
        System.out.println("calcularICBiblioteca");
        double[] vetorProprio = null;
        double expResult = 0.0;
//        double result = Projeto.calcularICBiblioteca(vetorProprio);
//        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

   

    
    
    public void testeObterValorProprio()
    {
        System.out.println("obterValorPropio");
        double[][] matriz = {{1,1,1},{1,1,2},{1,1,1}};
        double expResult = 2;
        double result = AHP.obterValorProprio(matriz,matriz.length);
        assertEquals(expResult, result, 0.0);
    }
    
    public void testObterVetorPropio()
    {
        System.out.println("obterVetorProprio");
        double[][] matrizA = {{1, 1, 1}, {2, 2, 2}, {3, 4, 5}};
        double[][] matrizB = {{1, 2, 3}, {4, 2, 3}, {5, 6, 7}};
        double valorProprio = 1;
        double[] expResult = {1, 2, 3};
        double[] result = AHP.obterVetorProprio(matrizA, matrizB, valorProprio);
        assertArrayEquals(expResult, result, valorProprio);

    }

    public void testCalculaRC() {
        System.out.println("CalculaRC");
        double IC = 9;
        int ordem = 4;
        double expResult = 10;
        double result = AHP.calcularRC(IC, ordem);
        assertEquals(expResult, result, 0.0);

    }

    public void testCalculaIC() {
        System.out.println("CalculaIC");
        double valorMax = 3;
        int ordem = 2;
        double expResult = 1;
        double result = AHP.calcularIC(valorMax, ordem);
        assertEquals(expResult, result, 0.0);
    }
    public void testMelhorAlternativa()
    {
        System.out.println("melhorAlternativa");
        double[][] matPrioridades = null;
        int nCriterios = 0;
        int nAlternativas = 0;
        double[] vetProprioCriterios = null;
        double[] vetPComposta = null;
        String[] alt = null;
        String expResult = "";
        String result = AHP.melhorAlternativa(matPrioridades, nCriterios, nAlternativas, vetProprioCriterios, vetPComposta, alt);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of aviso method, of class Projeto.
     */
    @Test
    public void testAviso()
    {
        System.out.println("aviso");
        AHP.aviso();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of inicio method, of class Projeto.
     */
    @Test
    public void testInicio()
    {
        System.out.println("inicio");
        AHP.inicio();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class Projeto.
     */
    @Test
    public void testMain() throws Exception
    {
        System.out.println("main");
        String[] args = null;
        Projeto.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
