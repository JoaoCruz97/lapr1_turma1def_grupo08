/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * 
 */
public class TOPSIS
{

    public static Scanner ler = new Scanner(System.in);

    /**
     *
     * @param ficheiro, nome do ficheiro de input vAlternativas guarda num vetor
     * as alternativas numeroAlternativas guarda a quantidade de alternativas
     * @throws FileNotFoundException
     */
    public static int alternativas(String ficheiro) throws FileNotFoundException
    {
        int numeroAlternativas = 0;
//        System.out.println("Qual o nome do ficheiro de input?");
//        String ficheiro = ler.nextLine();
        String[] linha = new String[1000];
        String[] vAlternativas = new String[1000];
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("alt"))
            {
                for (int i = 1; i < linha.length; i++)
                {
                    vAlternativas[i - 1] = linha[i];
                    numeroAlternativas++;

                }
                System.out.println("Alternativas:");
                for (int k = 0; k < numeroAlternativas; k++)
                {
                    System.out.println(vAlternativas[k]);
                }
            }
            }
        }
        return numeroAlternativas;
    }

    public static void alternativas(String ficheiro, String[] alternativas) throws FileNotFoundException
    {
        int numeroAlternativas = 0;
//        System.out.println("Qual o nome do ficheiro de input?");
//        String ficheiro = ler.nextLine();
        String[] linha = new String[1000];
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("alt"))
            {
                for (int i = 1; i < linha.length; i++)
                {
                    alternativas[i - 1] = linha[i];
                    numeroAlternativas++;

                }

            }
        }
        }
    }
//Guarda num vetor os criterios beneficios

    /**
     *
     * @param ficheiro beneficios guarda num vetor os criterios beneficios
     * criterioCusto guarda num vetor os criterios custo
     * @throws FileNotFoundException
     */
    public static int criteriosBeneficios(String ficheiro) throws FileNotFoundException
    {
        String[] linha = new String[1000];
//        String[] criterioCusto = new String[1000];
        int cont = 0;
//        int cont2 = 0;
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("crt_beneficio"))
            {
                for (int i = 1; i < linha.length; i++)
                {
                    cont++;

                }

            }
        }
        }
        return cont;
    }

    public static void criteriosBeneficios(String ficheiro, String[] beneficios) throws FileNotFoundException
    {
        String[] linha = new String[1000];
        int cont = 0;
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("crt_beneficio"))
            {
                for (int i = 1; i < linha.length; i++)
                {
                    beneficios[i - 1] = linha[i];
                    cont++;

                }

            }
        }
        }
    }

    public static int criteriosCustos(String ficheiro) throws FileNotFoundException
    {
        String[] linha = new String[1000];
        int cont = 0;
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("crt_custo"))
            {
                for (int i = 1; i < linha.length; i++)
                {
                    cont++;

                }

            }
        }
        }
        return cont;
    }

    public static void criteriosCustos(String ficheiro, String[] custos) throws FileNotFoundException
    {
        String[] linha = new String[1000];
        int cont = 0;
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("crt_custo"))
            {
                for (int i = 1; i < linha.length; i++)
                {
                    custos[i - 1] = linha[i];

                }

            }
        }
        }
    }
    // Guarda  os criterios e os pesos de cada um 

    /**
     *
     * @param ficheiro
     * @param nCriterios, numero de criterios criterios, guarda num vetor os
     * criterios
     * @return nCriterios
     * @throws FileNotFoundException
     */
    public static int criterios(String ficheiro) throws FileNotFoundException
    {
        int nCriterios = 0;
        String linha[] = new String[1000];
        String criterios[] = new String[1000];
//        System.out.println("Qual o nome do ficheiro de input?");
//        String ficheiro = ler.nextLine();
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("vec_pesos"))
            {
                for (int y = 1; y < linha.length; y++)
                {
                    criterios[y - 1] = linha[y];
                    nCriterios++;
                }

            }
        }
        }
        return nCriterios;
    }

    public static void criteriosFill(String ficheiro, String[] criterios) throws FileNotFoundException
    {
        int nCriterios = 0;
        String linha[] = new String[1000];
//        System.out.println("Qual o nome do ficheiro de input?");
//        String ficheiro = ler.nextLine();
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("vec_pesos"))
            {
                for (int y = 1; y < linha.length; y++)
                {
                    criterios[y - 1] = linha[y];
                    nCriterios++;
                }

            }
        }
        }
    }

    /**
     *
     * @param ficheiro
     * @param nCriterios pesos guarda num vetor de doubles os valores dos pesos
     * caso nao sejam inseridos pesosCriterios guarda num vetor de string os
     * pesos se forem inseridos corretamente
     * @throws FileNotFoundException
     */
    public static void pesos(String ficheiro, int nCriterios, double[] pesos) throws FileNotFoundException
    { //LÊ E ATRIBUI OS PESOS A CADA CRITÉRIO
        double pesoCriterio = (100 / nCriterios) * 0.01;
        DecimalFormat df = new DecimalFormat("0.###");
        String peso = df.format(pesoCriterio);
        String linha[] = new String[1000];
        String pesosCriterios[] = new String[10000];
//        System.out.println("Qual o nome do ficheiro de input?");
//        String ficheiro = ler.nextLine();
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
            if(linha.length!=0){
            if (linha[0].equalsIgnoreCase("vec_pesos"))
            {
                linha = in.nextLine().split(" +");
                if (linha.length < nCriterios)
                {             // deveria ser igual a zero mas só assim funciona
                    System.out.println("Pesos não foram atribuidos por isso serão distribuidos uniformemente ");
                    for (int y = 0; y < nCriterios; y++)
                    {
                        pesos[y] = pesoCriterio;
                    }
                    for (int y = 0; y < nCriterios; y++)
                    {
                        System.out.println(pesos[y]);
                    }
                } else
                {

                    for (int y = 0; y < linha.length; y++)
                    {
                        pesos[y] = Double.parseDouble(linha[y]);

                    }

                }
            }
        }
        }
    }

    /**
     *
     * @param ficheiro guarda no vetor de vetores matrizDecisao a matriz decisao
     * do ficheiro de texto
     * @throws FileNotFoundException
     * @throws java.text.ParseException
     */
    public static double[][] matrizDecisão(String ficheiro, int nAlternativas, int nCriterios) throws FileNotFoundException
    { //LÊ A MATRIZ DECISÃO
        double[][] matrizDecisao = new double[nAlternativas][nCriterios];
//        System.out.println("Qual o nome do ficheiro de input?");
//        String ficheiro = ler.nextLine();
        String[] linha = new String[nAlternativas];
        double elemento;
        String aux = null;
        Scanner in;
        in = new Scanner(new File(ficheiro));
        while (in.hasNext())
        {
            linha = in.nextLine().split(" +");
            if (linha.length > 0)
            {
                if(linha.length!=0){
                if (linha[0].equalsIgnoreCase("alt"))
                {
                    int linhas = 0;
                    while (in.hasNext())
                    {
                        linha = in.nextLine().split(" +");
                        for (int i = 0; i < linha.length; i++)
                        {
                            aux = linha[i];
                            elemento = Double.valueOf(aux);
                            matrizDecisao[linhas][i] = elemento;

                        }
                        linhas++;

                    }
                }

            }
        }
        }
        return matrizDecisao;

    }

    /**
     *
     * @param matriz matriz inicial
     * @param matrizQuadrado matriz inicial com todos os elementos ao quadrado
     */
    public static void matrizQuadrado(double[][] matriz, double[][] matrizQuadrado)
    {
        for (int i = 0; i < matriz.length; i++)
        {
            for (int j = 0; j < matriz[0].length; j++)
            {
                matrizQuadrado[i][j] = Math.pow(matriz[i][j], 2);
            }
        }
    }

    /**
     *
     * @param matriz recebe uma matriz como parametro
     * @param linhas numero de linhas da matriz
     * @param colunas numero colunas da matriz
     * @return um vetor com a soma de cada coluna
     */
    public static double[] somarColunas(double[][] matriz, int linhas, int colunas)
    {
        double somaColunas[] = new double[linhas];
        for (int i = 0; i < linhas; i++)
        {
            for (int j = 0; j < colunas; j++)
            {
                somaColunas[i] = matriz[j][i] + somaColunas[i];
            }
        }
        return somaColunas;
    }

    /**
     *
     * @param matriz recebe uma matriz como parametro
     * @param linha numero de linhas da matriz
     * @param coluna numero colunas da matriz
     * @return retorna o somatorio de cada coluna da matriz elevado a 1/2
     */
    public static double[] somatorioElevado(double[][] matriz, int linha, int coluna)
    {
        double[] somatorioElevado = new double[coluna];
        double[] col = new double[coluna];
        col = somarColunas(matriz, linha, coluna);
        for (int i = 0; i < linha; i++)
        {

            somatorioElevado[i] = Math.pow(col[i], (double) (0.5));

        }
        return somatorioElevado;
    }

    /**
     *
     * @param matriz recebe a matriz inicial
     * @param linha numero de linhas da matriz
     * @param coluna numero de colunas da matriz
     * @param matrizNormalizada matriz inicial com cada elemento dividido pelo
     * somatorio elevado
     */
    public static void normalizarMatriz(double[][] matriz, int linha, int coluna, double matrizNormalizada[][])
    {
        double[] somatorioElevado = new double[coluna];
        double[][] matrizQuadrada = new double[linha][coluna];
        matrizQuadrado(matriz, matrizQuadrada);
        somatorioElevado = somatorioElevado(matrizQuadrada, linha, coluna);

        for (int i = 0; i < linha; i++)
        {
            for (int j = 0; j < coluna; j++)
            {
                matrizNormalizada[i][j] = (double) matriz[i][j] / somatorioElevado[j];
            }
        }
    }
    
    /**
     * 
     * @param matrizNormalizada matriz inicial normalizada
     * @param linha numero de linhas
     * @param coluna numero de colunas
     * @param peso vetor de pesos
     * @param matrizPesada matriz normalizada com os elementos multiplicados pelo peso respetivo
     */
    public static void matrizPesada(double[][] matrizNormalizada, int linha, int coluna, double[] peso, double matrizPesada[][])
    {
        for (int i = 0; i < linha; i++)
        {
            for (int j = 0; j < coluna; j++)
            {
                matrizPesada[i][j] = (double) matrizNormalizada[i][j] * peso[j];
            }
        }
    }

    /**
     * 
     * @param matrizPesada recebe a matriz pesada como parametro
     * @param beneficios vetor dos criterios beneficio
     * @param custos vetor dos criterios custo
     * @param vetorIdeal vetor com os maiores valores de cada coluna
     */
    public static void soluçaoIdeal(double[][] matrizPesada, String[] beneficios, String[] custos, double vetorIdeal[])
    {

        for (int j = 0; j < beneficios.length; j++)
        {
            vetorIdeal[j] = 0;
            for (int i = 0; i < matrizPesada.length; i++)
            {
                if (vetorIdeal[j] < matrizPesada[i][j])
                {
                    vetorIdeal[j] = matrizPesada[i][j];
                }
            }

        }

        for (int j = (matrizPesada.length - custos.length); j < matrizPesada.length; j++)
        {
            vetorIdeal[j] = 1;
            for (int i = 0; i < matrizPesada.length; i++)
            {
                if (vetorIdeal[j] > matrizPesada[i][j])
                {
                    vetorIdeal[j] = matrizPesada[i][j];
                }
            }
        }

    }

    /**
     * 
     * @param matriz matriz Pesada
     * @param beneficios vetor com os criterios bneficio
     * @param custos vetor com os criterios custo
     * @param vetorNaoIdeal vetor com os elementos menores de cada coluna
     */
    public static void soluçaoNaoIdeal(double[][] matriz, String[] beneficios, String[] custos, double vetorNaoIdeal[])
    {
        for (int j = 0; j < beneficios.length; j++)
        {
            vetorNaoIdeal[j] = 1;
            for (int i = 0; i < matriz.length; i++)
            {
                if (vetorNaoIdeal[j] > matriz[i][j])
                {
                    vetorNaoIdeal[j] = matriz[i][j];
                }
            }

        }

        for (int j = (matriz.length - custos.length); j < matriz.length; j++)
        {
            vetorNaoIdeal[j] = 0;
            for (int i = 0; i < matriz.length; i++)
            {
                if (vetorNaoIdeal[j] < matriz[i][j])
                {
                    vetorNaoIdeal[j] = matriz[i][j];
                }
            }
        }
    }
    
    /**
     * 
     * @param matNormalizadaPesada 
     * @param matSeparacao matriz de sepraçao ideal/ideal negativa
     * @param vetor vetor Ideal ou idealNegativo
     * @param separacao ideal ou idealNegativa
     */
    public static void separacaoIdealENaoIdeal(double[][] matNormalizadaPesada, double[][] matSeparacao, double[] vetor, double[] separacao)
    {
        for (int i = 0; i < matNormalizadaPesada.length; i++)
        {
            for (int j = 0; j < matNormalizadaPesada[0].length; j++)
            {
                matSeparacao[i][j] = Math.pow((matNormalizadaPesada[i][j] - vetor[j]), 2);
            }
        }
        for (int i = 0; i < matSeparacao.length; i++)
        {
            for (int j = 0; j < matSeparacao[0].length; j++)
            {
                separacao[i] += matSeparacao[i][j];
            }
        }
        for (int i = 0; i < separacao.length; i++)
        {
            separacao[i] = Math.pow(separacao[i], 0.5);
        }
    }

    /**
     * 
     * @param sepIdeal 
     * @param sepNaoIdeal
     * @return 
     */
    public static double[] proxRelativa(double[] sepIdeal, double[] sepNaoIdeal)
    {
        double[] proximidade = new double[sepIdeal.length];
        for (int i = 0; i < sepIdeal.length; i++)
        {
            proximidade[i] = sepNaoIdeal[i] / (sepIdeal[i] + sepNaoIdeal[i]);
        }
        return proximidade;
    }

    /**
     * 
     * @param proximidade
     * @return posiçao no vetor da melhor alternativa
     */
    public static int melhorAlternativa(double[] proximidade)
    {

        double valorAlternativa = proximidade[0];
        int pos = 0;

        for (int j = 1; j < proximidade.length; j++)
        {
            if (proximidade[j] > valorAlternativa)
            {
                valorAlternativa = proximidade[j];
                pos = j;
            }
        }
        return pos;
    }
    
    /**
     * 
     * @param proximidade
     * @param alternativas vetor com as alternativas
     * @return a string da melhor alternativa
     */
    public static String altEscolhida(double[] proximidade, String[] alternativas)
    {
        int pos = melhorAlternativa(proximidade);
        String alternativaEscolhida = alternativas[pos];
        return alternativaEscolhida;

    }

    public static void escrever(double[] vetPesos, double[][] matDecisao,
            double[][] matNormalizada, double[][] matNormalizadaPesada,
            double[] solucaoIdeal, double[] solucaoIdealNegativa, double[] distanciaIdeal,
            double[] distanciaIdealNegativa, double[] proximidade, String[] criterios, String[] beneficios,
            String[] custos, String[] alternativas, String altEscolhida, int Cri, int Alt, String melhor, String fichOut) throws FileNotFoundException
    {
        Formatter write = new Formatter(new File(fichOut));
        {
            write.format("Critérios: %n");
            System.out.printf("Critérios: %n");
            for (int i = 0; i < criterios.length; i++)
            {
                write.format("%s%n", criterios[i]);
                System.out.printf("%s%n", criterios[i]);
            }

            write.format("Critérios benefício: %n");
            System.out.printf("Critérios benefício: %n");
            for (int i = 0; i < beneficios.length; i++)
            {
                write.format("%s%n", beneficios[i]);
                System.out.printf("%s%n", beneficios[i]);
            }

            write.format("Critérios custo: %n");
            System.out.printf("Critérios custo: %n");
            for (int i = 0; i < custos.length; i++)
            {
                write.format("%s%n", custos[i]);
                System.out.printf("%s%n", custos[i]);
            }

            write.format("Alternativas: %n");
            System.out.printf("Alternativas: %n");
            for (int i = 0; i < alternativas.length; i++)
            {
                write.format("%s%n", alternativas[i]);
                System.out.printf("%s%n", alternativas[i]);
            }

            write.format("Matriz de entrada: %n");
            System.out.printf("Matriz de entrada: %n");
            for (int i = 0; i < Alt; i++)
            {
                for (int j = 0; j < Cri; j++)
                {
                    write.format(matDecisao[i][j] + "  ");
                    System.out.printf(matDecisao[i][j] + "  ");
                }
                write.format("%n");
                System.out.println();
            }

            write.format("Vetor de pesos: %n");
            System.out.printf("Vetor de pesos: %n");
            for (int i = 0; i < vetPesos.length; i++)
            {
                write.format("%f%n", vetPesos[i]);
                System.out.printf("%f%n", vetPesos[i]);
            }

            write.format("Matriz normalizada: %n");
            for (int i = 0; i < matNormalizada.length; i++)
            {
                for (int j = 0; j < matNormalizada[0].length; j++)
                {
                    write.format(matNormalizada[i][j] + "  ");
                }
                write.format("%n");
            }

            write.format("Matriz normalizada pesada: %n");
            System.out.printf("Matriz normalizada pesada: %n");
            for (int i = 0; i < matNormalizadaPesada.length; i++)
            {
                for (int j = 0; j < matNormalizadaPesada[0].length; j++)
                {
                    write.format(matNormalizadaPesada[i][j] + "  ");
                    System.out.printf(matNormalizadaPesada[i][j] + "  ");
                }
                write.format("%n");
                System.out.println();
            }

            write.format("Solução ideal: %n");
            for (int i = 0; i < solucaoIdeal.length; i++)
            {
                write.format("%f%n", solucaoIdeal[i]);
            }

            write.format("Solução ideal negativa: %n");
            for (int i = 0; i < solucaoIdealNegativa.length; i++)
            {
                write.format("%f%n", solucaoIdealNegativa[i]);
            }

            write.format("Distância entre a solução ideal e a alternativa: %n");
            for (int i = 0; i < distanciaIdeal.length; i++)
            {
                write.format("%f%n", distanciaIdeal[i]);
            }

            write.format("Distância entre a solução ideal negativa e a alternativa: %n");
            for (int i = 0; i < distanciaIdealNegativa.length; i++)
            {
                write.format("%f%n", distanciaIdealNegativa[i]);
            }

            write.format("Vetor proximidades: %n");
            System.out.printf("Vetor proximidades: %n");
            for (int i = 0; i < proximidade.length; i++)
            {
                write.format("%f%n", proximidade[i]);
                System.out.printf("%f%n", proximidade[i]);
            }

            write.format("Alternativa final:" + melhor);
            System.out.printf("Alternativa final:" + melhor);

        }
        write.close();
    }

    public static void TOPSIS(String ficheiro,String fichOut) throws FileNotFoundException, ParseException
    {
        
        
        int nCriterios = criterios(ficheiro);
        Formatter fOut = new Formatter(new File(fichOut));
        String[] criterios = new String[nCriterios];
        criteriosFill(ficheiro, criterios);
        int nAlternativas = alternativas(ficheiro);
        int criteriosBeneficios = criteriosBeneficios(ficheiro);
        String[] beneficios = new String[criteriosBeneficios];
        criteriosBeneficios(ficheiro, beneficios);
        int criteriosCustos = criteriosCustos(ficheiro);
        String[] custos = new String[criteriosCustos];
        criteriosCustos(ficheiro, custos);

        double[] vetPesos = new double[nCriterios];
        pesos(ficheiro, nCriterios, vetPesos);
        double[][] matriz = new double[nAlternativas][nCriterios];
        matriz = matrizDecisão(ficheiro, nAlternativas, nCriterios);

        double[][] matrizQuadrado = new double[nAlternativas][nCriterios];
        double matrizNormalizada[][] = new double[nAlternativas][nCriterios];
        double matrizPesada[][] = new double[nAlternativas][nCriterios];
        double[] vetorIdeal = new double[nCriterios];
        double[] vetorNaoIdeal = new double[nCriterios];
        double[][] matSeparacao = new double[nAlternativas][nCriterios];
        double[] separacao = new double[nAlternativas];
        double[] sepIdeal = new double[nAlternativas];
        double[] sepNaoIdeal = new double[nAlternativas];
        double[] proximidade = new double[nAlternativas];

        String[] alternativas = new String[nAlternativas];
        alternativas(ficheiro, alternativas);
        matrizQuadrado(matriz, matrizQuadrado);
        normalizarMatriz(matriz, nAlternativas, nCriterios, matrizNormalizada);
        matrizPesada(matrizNormalizada, nAlternativas, nCriterios, vetPesos, matrizPesada);
        soluçaoIdeal(matrizPesada, beneficios, custos, vetorIdeal);
        soluçaoNaoIdeal(matrizPesada, beneficios, custos, vetorNaoIdeal);
        separacaoIdealENaoIdeal(matrizPesada, matSeparacao, vetorIdeal, sepIdeal);
        separacaoIdealENaoIdeal(matrizPesada, matSeparacao, vetorNaoIdeal, sepNaoIdeal);

        proximidade = proxRelativa(sepIdeal, sepNaoIdeal);
        melhorAlternativa(proximidade);
        String melhor = altEscolhida(proximidade, alternativas);

        escrever(vetPesos, matriz, matrizNormalizada, matrizPesada, vetorIdeal, vetorNaoIdeal, sepIdeal, sepNaoIdeal, proximidade, criterios, beneficios, custos, alternativas, ficheiro, nCriterios, nAlternativas, melhor, fichOut);

    }
}
