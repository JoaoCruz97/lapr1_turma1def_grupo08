/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

import java.io.FileNotFoundException;
import java.text.ParseException;
import static projeto.AHP.mainAhp;
import static projeto.TOPSIS.TOPSIS;

/**
 *
 * @author rmmbg
 */
public class Projeto {

    public static void main(String[] args) throws FileNotFoundException, ParseException {
        String op = null;
        String lim = null;
        String fich = null;
        String fichOut = null;

        if (args.length == 4) {
            op = args[0];
            lim = args[1];
            fich = args[2];
            fichOut = args[3];
            System.out.println(op);
            System.out.println(lim);
            System.out.println(fich);
            System.out.println(fichOut);
            
            if ("M".equals(op) && "1".equals(lim)) {
                System.out.println("METODO AHP");
                double limRC = 0.1;
                double limC = 0;
                mainAhp(fich, fichOut,limC, limRC);
            } else if ("M".equals(op) && "2".equals(lim)) {
                System.out.println("TOPSIS");
                TOPSIS(fich, fichOut);
            } else if ("L".equals(op)) {
                double limiar = Double.parseDouble(lim);
                        if(0 <= limiar &&limiar < 1 ){
                            System.out.println("AHP COM LIMIAR DE CRITERIOS");
                            double limRC = 0.1;
                            double limC = limiar;
                            
                            mainAhp(fich, fichOut,limC, limRC);
                        }else{
                         System.out.println("VALOR DE LIMIAR ERRADO");
                        }
            }else if("S".equals(op)){
             double limiar = Double.parseDouble(lim);
                        if(0 < limiar &&limiar < 1 ){
                            System.out.println("AHP COM LIMIAR DE CRITERIOS");
                            double limC = 0;
                            double limRC = limiar;
                            mainAhp(fich, fichOut,limC, limRC);
                        }else{
                         System.out.println("VALOR DE LIMIAR ERRADO");
                         
                        }
            
            }
        } else {
            System.out.println("NUMERO DE ARGUMENTOS INVALIDO");
        }

    }

}
