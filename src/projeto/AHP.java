/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author rmmbg
 */
public class AHP {
// A ORDEM DA MATRIZ N_CRITERIOS E A ORDEM DA MATRIZ DOS CRITERIOS E N_ALTERNATIVAS E A ORDEM DAS MATRIZES DE COMPARAÇAO

    /**
     * INPUT
     */
    public static Scanner ler = new Scanner(System.in);

    ;
    
   
//    /**
//     *
//     *
//     * @param fraction
//     * @param temp uma string que vai ser convertida para double
//     * @return o double correspondente a essa string
//     * @throws java.text.ParseException
//     */
    public static Double converterParaDouble(String temp) throws ParseException {
        Double aux = null;
        if (temp != null) {
            if (temp.contains("/")) {
                String[] num = temp.split("/");
                if (num.length == 2) {
                    BigDecimal d1 = BigDecimal.valueOf(Double.valueOf(num[0]));
                    BigDecimal d2 = BigDecimal.valueOf(Double.valueOf(num[1]));
                    BigDecimal response = d1.divide(d2, MathContext.DECIMAL128);
                    aux = response.doubleValue();
                }
            } else {
                aux = Double.valueOf(temp);
            }
        }
        if (aux == null) {
            throw new ParseException(temp, 0);
        }
        return aux;
    }

    /**
     * METODO PARA TRANSPOR MATRIZ
     *
     * @param mat matriz a ser transposta
     * @param matT matriz transposta
     * @param N_CRITERIOS numero de criterios
     * @param N_ALTERNATIVAS numero de alternativas
     */
    public static void transpor(double[][] mat, double[][] matT, int N_CRITERIOS, int N_ALTERNATIVAS) {

        for (int i = 0; i < N_CRITERIOS; i++) {
            for (int j = 0; j < N_ALTERNATIVAS; j++) {
                matT[j][i] = mat[i][j];
            }
        }

    }

    /**
     * IMPRIMIR MATRIZ PARA UM FICHEIRO
     *
     * @param matriz matriz a ser imprimida
     * @param fOut FORMATTER
     * @throws ParseException
     */
    public static void impressaoMatriz(double[][] matriz, Formatter fOut) throws ParseException {
        fOut.format("%n");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {

                fOut.format(" " + Double.toString(matriz[i][j]));
            }
            fOut.format("%n");
        }
    }

    /**
     * METODO DE IMPRESSAO DE VETOR PARA UM FICHEIRO
     *
     * @param limite e a ordem do vet.
     * @param vec vetor a imprimir
     * @param fOut fORMATTER
     * @throws ParseException
     */
    public static void impressaoVet(double[] vec, int limite, Formatter fOut) throws ParseException {
        fOut.format("%n");

        for (int i = 0; i < limite; i++) {

            fOut.format(" " + Double.toString(vec[i]));

        }
    }

// metodo de imprimir matrizes com a biblioteca laj4
    /**
     * imprimir matriz
     *
     * @param mat matriz a imprimir
     */
    public static void printMat(double[][] mat) {

        // Criar  Matriz
        Matrix m = new Basic2DMatrix(mat);
        // Imprimir a matriz
        System.out.println(m);
    }

    /*O metodo somar colunas cria um vetor auxiliar, sendo este a soma linhas de cada coluna da matriz
 *ESTE PROCEDIMENTO E NECESSARIO PARA NORMALIZAR O VETOR  
 *@mat e a matriz com os elementos que vão ser comparados
 *@ordem e a ordem da matriz mat 
 *@somaColunas é o vetor auxiliar das somas das linhas de cada coluna
     */
    public static void somarColunas(double mat[][], int linhas, int colunas, double somaColunas[]) {
        for (int i = 0; i < colunas; i++) {
            for (int j = 0; j < linhas; j++) {
                somaColunas[i] = somaColunas[i] + mat[j][i];
            }
        }
    }

    /**
     * METODO PARA NORMALIZAR UMA MATRIZ
     *
     * @param mat matriz a normalizar
     * @param linha linhas da matriz
     * @param coluna colunas da matriz
     * @param matNormalizada matriz normalizada
     */
    public static void normalizarMatriz(double mat[][], int linha, int coluna, double matNormalizada[][]) {
        double[] aux = new double[coluna];

        somarColunas(mat, linha, coluna, aux);

        for (int i = 0; i < linha; i++) {
            for (int j = 0; j < coluna; j++) {
                matNormalizada[i][j] = (double) mat[i][j] / aux[j];
            }
        }
    }

    /**
     * METODO PARA PREENCHER O VETOR PROPRIO DE MUMA MATRIZ
     *
     * @param matNormalizada matriz normalizada
     *
     * @param vetProp vetor proprio da matriz
     */
    public static void criarVetPropAprox(double[][] matNormalizada, double[] vetProp) {

        double[] soma = new double[vetProp.length];
        for (int i = 0; i < vetProp.length; i++) {
            for (int j = 0; j < vetProp.length; j++) {
                soma[i] = soma[i] + matNormalizada[i][j];
                //faz a soma de cada linha
            }
        }
        for (int i = 0; i < vetProp.length; i++) {
            vetProp[i] = soma[i] / vetProp.length;
            //calcula os vetores prioridade 
        }
    }

    //JUNÇAO DOS METODOS QUE FORNECEM OS VALORES PROPEIOS APROXIMADOS
    public static void valorPropAprox(double[][] mat, int linha, int coluna, double[] vetProp) {
        double[][] matNormalizada = new double[linha][coluna];

        normalizarMatriz(mat, linha, coluna, matNormalizada);
        criarVetPropAprox(matNormalizada, vetProp);
//        System.out.println("VALORES PROPRIOS:");
//        for (int i = 0; i < 3; i++) {
//            System.out.println(vetProp[i]);
//        }
    }

    //OS DOIS METODOS EM CIMA SAO METODOs DE INVOCAÇOES,NAO NECESSITAM DE TESTES!
    /*
    
    /**
     * calculoValorProprioEVetorProprioExato é um método que obtém o valor
     * proprio e vetor proprio usando a biblioteca la4j
     *
     * @param limite dimensões da matriz
     * @param matriz recebe uma matriz introduzida
     * @return o Vetor proprio que contêm as médias das linhas da matriz
     * introduzida
     */
    public static double[] calculoValorProprioEVetorProprioExato(double matriz[][], int limite) {
        Matrix a = new Basic2DMatrix(matriz);
        EigenDecompositor eigen = new EigenDecompositor(a);
        Matrix[] mat = eigen.decompose();
        double[][] matVetorProprio = mat[0].toDenseMatrix().toArray();
        double[][] matValorProprio = mat[1].toDenseMatrix().toArray();
        double valorProprio = obterValorProprio(matValorProprio, limite);
        double[] vetorProprio = obterVetorProprio(matVetorProprio, matValorProprio, valorProprio);
        return vetorProprio;
    }

    /**
     * obterValorProprio é um método que vai retirar o valor proprio duma matriz
     *
     * @param matriz recebe uma matriz introduzida pelo utilizador
     * @param limite Dimensoes do vetor
     * @return o valor maximo da matriz, que é o valor proprio
     */
    public static double obterValorProprio(double matriz[][], int limite) {
        double max = 0;

        for (int i = 0; i < limite; i++) {
            for (int j = 0; j < limite; j++) {
                if (matriz[i][j] > max) {
                    max = matriz[i][j];

                }

            }
        }
        return max;

    }

    /**
     *
     * @param matrizA é a primeira matriz produzida depois do uso da biblioteca,
     * que contem o vetor proprio
     * @param matrizB é a segunda matriz resultante do uso da biblioteca, que
     * contem o valor proprio
     * @param valorProprio é o valor maximo da segunda matriz
     * @return vetor proprio que é o vetor da primeira matriz que se encontra na
     * mesma coluna do valor proprio(segunda matriz)
     */
    public static double[] obterVetorProprio(double matrizA[][], double matrizB[][], double valorProprio) {
        int col = 0;
        for (int i = 0; i < matrizB.length; i++) {
            for (int j = 0; j < matrizB[0].length; j++) {
                if (valorProprio == matrizB[i][j]) {
                    col = j;
                }

            }
        }

        double vetorProprio[] = new double[matrizA[0].length + 1];
        for (int i = 0; i < matrizA[0].length; i++) {

            vetorProprio[i] = matrizA[i][col];

        }
        for (int k = 0; k < vetorProprio.length; k++) {
            if (vetorProprio[k] < 0) {
                vetorProprio[k] = vetorProprio[k] * -1;
            }
        }
        double soma = 0;
        for (int k = 0; k < vetorProprio.length; k++) {

            soma = soma + vetorProprio[k];
        }
        for (int k = 0; k < vetorProprio.length; k++) {

            vetorProprio[k] = vetorProprio[k] / soma;
        }
        return vetorProprio;
    }
    // CONSISTENCIA 

    /**
     * calcula o IR de uma matriz com base na sua ordem
     *
     * @param ordemMat ORDEM DA MATRIZ
     * @return
     */
    public static double calcularIR(int ordemMat) {
        double IR = 0;
        if (ordemMat == 1) {
            IR = 0;
        }
        if (ordemMat == 2) {
            IR = 0;
        }
        if (ordemMat == 3) {
            IR = 0.58;
        }
        if (ordemMat == 4) {
            IR = 0.9;
        }
        if (ordemMat == 5) {
            IR = 1.12;
        }
        if (ordemMat == 6) {
            IR = 1.24;
        }
        if (ordemMat == 7) {
            IR = 1.32;
        }
        if (ordemMat == 8) {
            IR = 1.41;
        }
        if (ordemMat == 9) {
            IR = 1.45;
        }
        if (ordemMat == 10) {
            IR = 1.49;
        }
        if (ordemMat == 11) {
            IR = 1.51;
        }
        if (ordemMat == 12) {
            IR = 1.48;
        }

        if (ordemMat == 13) {
            IR = 1.56;
        }
        if (ordemMat == 14) {
            IR = 1.57;
        }
        if (ordemMat == 15) {
            IR = 1.59;
        }
        return IR;
    }

    /**
     * METODO DE CALCULO DO LAMBDA MAXIMO (MAIOR VALOR PROPRIO)
     *
     * @param matriz matriz utilizada
     * @param vetorP vetor proprio da matriz
     * @param linhas ordem da matriz
     * @return
     */
    public static double calcularLamb(double[][] matriz, double[] vetorP, int linhas) {
        double[] temp = new double[linhas];
        double lambda = 0;
        for (int i = 0; i < linhas; i++) {
            temp[i] = 0;
        }
        for (int a = 0; a < linhas; a++) {
            for (int j = 0; j < linhas; j++) {
                temp[a] = temp[a] + (matriz[a][j] * vetorP[j]);
            }
        }
        for (int b = 0; b < linhas; b++) {
            lambda = lambda + (temp[b] / vetorP[b]);
        }
        lambda = lambda / linhas;
        return lambda;
    }

    /**
     *
     * @param lambda lambda maximo de uma matriz
     * @param ordem a sua ordem
     * @return o Indice de consistencia
     */
    public static double calcularIC(double lambda, int ordem) {
        double IC = ((lambda - ordem) / (ordem - 1));

        return IC;
    }

    /**
     *
     * @param IC INDICE DE CONSISTENCIA
     * @param IR IR DA MESMA MATRIZ
     * @return RAZAO DE CONSISTENCIA
     */
    public static double calcularRC(double IC, double IR) {
        double RC = IC / IR;

        return RC;
    }

    /**
     *
     * @param ordem ordem de uma matriz
     * @param matrizComp matriz composta dos vetores prioridade
     * @param vetorPr vetor proprio das matriz criterios
     * @param fOut formatter, para fazer output de dados
     * @return Razao de consistencia
     */
    public static double consistencia(int ordem, double[][] matrizComp, double[] vetorPr, Formatter fOut) {

        double ir = calcularIR(ordem);
        double lambda = calcularLamb(matrizComp, vetorPr, ordem);
        double ic = calcularIC(lambda, ordem);
        double rc = calcularRC(ic, ir);

        fOut.format("%n");
        fOut.format("IR: " + ir);
        fOut.format("%n");
        fOut.format("RC: " + rc);
        fOut.format("%n");
        fOut.format("MAIOR VALOR PROPRIO: " + lambda);
        fOut.format("%n");
        return rc;
    }

    //ESTUDAR QUAL A MELHOR ALTERNATIVA que pode ser o carro 1,2,3 ou o 4
    //a partir da multiplicação da matriz proprio pelo vetor proprio dos criterios
    /**
     *
     * @param matPrioridades matriz prioridades compstas
     * @param N_ALTERNATIVAS numero de alternativas
     * @param N_CRITERIOS NUMERO DE CRITERIOS
     *
     * @param vetPCriterios vetor proprio da matriz criterios
     * @param vetPComposta vetor prioridades compostas
     * @param alt alternativa
     * @return
     */
    public static String melhorAlternativa(double[][] matPrioridades, int N_ALTERNATIVAS, int N_CRITERIOS, double[] vetPCriterios, double[] vetPComposta, String[] alt) {
        String alternativa = "NULL";
        double maior = 0;
        for (int i = 0; i < N_ALTERNATIVAS; i++) {
            for (int j = 0; j < N_CRITERIOS; j++) {
                vetPComposta[i] = vetPComposta[i] + (matPrioridades[i][j] * vetPCriterios[j]);
            }
        }
        for (int i = 0; i < N_ALTERNATIVAS; i++) {
            if (vetPComposta[i] > maior) {
                maior = vetPComposta[i];
                alternativa = alt[i];

            }

        }

        return alternativa;

    }

    // O metodo de aviso imprime um conjunto de informaçoes importantes para a sua utilizaçao
    public static void aviso() {
        System.out.println("------------------------ATENÇAO LEIA A INFORMACAO A BAIXO------------------------"
                + "\n-AS ALTERNATIVAS NESTE PROGRAMA SAO APRESENTADAS COMO CARROx (x e um numero conforme a ordem no input)."
                + "\n-PARA O BOM FUNCIONAMENTO DO PROGRAMA:"
                + "\n-TODAS AS MATRIZES DEVEM TER O RESPETIVO TITULO:"
                + "\n  -mc_criterios criterio1 criterio2 criterio3;(MATRIZ CRITERIOS)"
                + "\n  -mpc_criterio.. car1 car2 car3 car4 (MATRIZ ALTERNATIVAS, carX sao as alternativas);"
                + "\n-OS CONSTINTUINTES DAS MATRIZES DEVEM ESTAR SEPARADOS POR ESPACOS."
                + "\n------------------------ATENÇAO LEIA A INFORMACAO A CIMA------------------------"
                + "\n");

    }

    // o metodo inicio e um metodo que imprime o nome do programa e ainda faz uma pausa, para que o user
    // tenha tempo para ler os avisos primindo enter para executar o resto do programa
    public static void inicio() {
        System.out.println("------PROGRAMA DE AUXILIO A ESCOLHA MULTICRITERIO------"
                + "\n------------PARA INCICIAR A APP PRIMA ENTER------------");
        ler.nextLine();
    }
    //INPUT

    //NUMERO DE ALTERNATIVAS/CRITERIOS
    // UTILIZA OS TITULOS ACIMA DAS MATRIZES.
    /**
     * Procura os nomes dos criterios e conta-os
     *
     * @param in input do ficheiro de leitura de dados
     * @return numero de criterios avaliados
     */
    public static int criterios(Scanner in) {
        int N_CRITERIOS = 0;
        while (in.hasNext()) {
            String linha = in.nextLine();
            linha = linha.trim();

            String[] temp = linha.split("  ");
            N_CRITERIOS = temp.length - 1;
            if (N_CRITERIOS > 0) {
                break;
            }

        }
        return N_CRITERIOS;
    }

    /**
     * Procura os nomes das alternativas e conta-as
     *
     * @param in input do ficheiro de leitura de dados
     * @return numero de criterios avaliados
     */
    public static int alternativas(Scanner in) {
        int N_ALTERNATIVAS = 0;//alternativas(in);
        int n = 0;//CONTADOR
        while (in.hasNext()) {

            String linha = in.nextLine();
            linha = linha.trim();
            if (linha.length() > 0) {
                char cont = 'm';
                char sub = linha.charAt(0);

                if (cont == sub) {
                    n++;
                }
                if (cont == sub && n > 1) {
                    String[] temp = linha.split("  ");
                    N_ALTERNATIVAS = temp.length - 1;
                    break;
                }
            }
        }

        return N_ALTERNATIVAS;
    }
    // METODOS DE CONTROLE DE LIMIARES

    /**
     * ESTE METODO RECEBE UUM FICHEIRO E CRIA AS MATRIZES COMPARAÇAO EM double
     * PARA O METODO AHP
     *
     * @param N_CRITERIOS NUMERO DE CRITERIOS AVALIADOS
     * @param N_ALTERNATIVAS NUMERO DE ESCOLHAS POSSIVEIS
     * @param mCrit matriz criterios
     * @param mAlt matrizes alternativas
     * @param fIn scanner de input de ficheiro
     * @param erro OUTPUT PARA FIXHIERO DE ERROS
     * @throws ParseException
     */
    public static void input(int N_CRITERIOS, int N_ALTERNATIVAS, double[][] mCrit, double[][][] mAlt, Scanner fIn, Formatter erro) throws ParseException {
        int nMat = 0;
        while (fIn.hasNext()) {

            String linha = fIn.nextLine();
            linha = linha.trim();

            if (linha.length() > 0) {

                char cont = 'm';
                char prim = linha.charAt(0);
                if (nMat == 0 && prim == cont) {

                    for (int i = 0; i < N_CRITERIOS; i++) {
                        String m = fIn.nextLine();
                        String[] temp = m.split(" +");

                        for (int j = 0; j < N_CRITERIOS; j++) {

                            mCrit[i][j] = converterParaDouble(temp[j]);

                        }
                    }
                    nMat++;

                } else if (prim == cont) {
                    for (int i = 0; i < N_ALTERNATIVAS; i++) {
                        String m = fIn.nextLine();
                        String[] temp = m.split(" +");

                        for (int j = 0; j < N_ALTERNATIVAS; j++) {
//                            if(temp[j]!=){
//                                erro.format()
//                            }

                            mAlt[nMat - 1][i][j] = converterParaDouble(temp[j]);

                        }
                    }
                    nMat++;

                } else {
                    erro.format("%n");
                    erro.format("ERRO NO FICHEIRO DE INPUT (linhas com carateres desnecessarios): ");

                    erro.format(linha);
                    erro.format("%n");
                }

            }

        }

    }

    /**
     * METODO PARA ALTERAR O NUMERO DE MATRIZES ALTERNATIVAS CASO CERTOS
     * CRITERIOS ESTAJAM ABAIXO DE UM LIMIAR DEFINIDO
     *
     * @param matriz matrizes alternativas
     * @param pos posiçao a ser descartada (CRITERIO ABAIXO DO LIMIAR)
     * @param nCriterios numero de criterios
     */
    public static void organizaMatrizAlt(double[][][] matriz, int pos, int nCriterios) {
        for (int i = pos; i < nCriterios - 1; i++) {
            matriz[i] = matriz[i + 1];
        }

    }

    public static void organizMCrit(double[][] mCrit, int pos, int N_CRITERIOS) {
        for (int i = 0; i < N_CRITERIOS; i++) {

            for (int k = pos; k < N_CRITERIOS; k++) {

                mCrit[i][k] = mCrit[i][k + 1];
            }
        }
        for (int k = pos; k < N_CRITERIOS; k++) {

            mCrit[k] = mCrit[k + 1];
        }

    }

    @SuppressWarnings("empty-statement")
    public static void mainAhp(String fich, String fichOut, double limC, double limRC) throws FileNotFoundException, ParseException {

        Formatter fOut;
        //OUTPUTS
        Formatter erro = new Formatter(new File("FicheiroErros.txt"));
        fOut = new Formatter(new File(fichOut));
        fOut.format("FICHEIRO DE OUTPUT:");
        fOut.format("%n");
        Date x = new Date();
        erro.format("FICHEIRO DE ERROS - " + x);
        erro.format("%n");
        //OUTPUTS

        Scanner in = new Scanner(new File(fich));
        //CONTAR CRITERIOS E ALTERNATIVAS
        Scanner in2 = new Scanner(new File(fich));
        Scanner in3 = new Scanner(new File(fich));
        int N_ALTERNATIVAS = alternativas(in2);
        int N_CRITERIOS = criterios(in3);
        if (N_CRITERIOS < 3) {
            System.out.println("IMPOSSIVEL CONTINUAR: numero de criterios insuficiente.");

        } else {
            String[] alt = new String[N_ALTERNATIVAS];
            for (int i = 0; i < N_ALTERNATIVAS; i++) {
                alt[i] = ("ALTERNATIVA" + (i + 1));
            }   //CONTAR CRITERIOS E ALTERNATIVAS
            double[][] mCrit = new double[N_CRITERIOS][N_CRITERIOS];
            double[][][] mAlt = new double[N_CRITERIOS][N_ALTERNATIVAS][N_ALTERNATIVAS];
            double[][] mCritNorm = new double[N_CRITERIOS][N_CRITERIOS];
            double[][][] mAltNorm = new double[N_CRITERIOS][N_ALTERNATIVAS][N_ALTERNATIVAS];
            double[] vetPCrit = new double[N_CRITERIOS];
            double[][] vetPAlt = new double[N_CRITERIOS][N_ALTERNATIVAS];
            double[][] vetPAltTrans = new double[N_ALTERNATIVAS][N_CRITERIOS];
            double[] vetPComposta = new double[N_ALTERNATIVAS];
            String alternativa;

            double rc;
            //INPUT
            Scanner in4 = new Scanner(new File(fich));
            input(N_CRITERIOS, N_ALTERNATIVAS, mCrit, mAlt, in4, erro);
            //INPUT

            //VERIFICAR LIMIAR CRITERIOS
            int[] vetPos = new int[N_CRITERIOS];//POSIÇOES DOS CRITERIOS ABAIXO DO LIMIAR
            normalizarMatriz(mCrit, N_CRITERIOS, N_CRITERIOS, mCritNorm);
            criarVetPropAprox(mCritNorm, vetPCrit);
            for (int j = 0; j < N_CRITERIOS; j++) {
                for (int i = 0; i < N_CRITERIOS; i++) {
                    mCritNorm[j][i] = 0;
                }
            }
            int nCLim = 0; //CRITERIOS ABAIXO DO LIMITE
            for (int j = 0; j < N_CRITERIOS; j++) {
                if (vetPCrit[j] < limC) {
                    vetPos[nCLim] = j;
                    nCLim++;

                }
            }

            if (nCLim > 0) {

                for (int j = 0; j < nCLim; j++) {
                    if (vetPos[j] == N_CRITERIOS) {
                        organizMCrit(mCrit, vetPos[j], N_CRITERIOS);
                    } else {
                        for (int r = 0; r < N_CRITERIOS; r++) {
                            for (int a = vetPos[j]; a < N_CRITERIOS; a++) {
                                mCrit[r][a] = 0;
                            }
                        }
                        for (int v = vetPos[j]; v < N_CRITERIOS; v++) {
                            for (int a = 0; a < N_CRITERIOS; a++) {
                                mCrit[v][a] = 0;
                            }
                        }
                    }
                    organizaMatrizAlt(mAlt, vetPos[j], N_CRITERIOS);
                }
                N_CRITERIOS = N_CRITERIOS - nCLim;
            }
            //IMPRESSAO MATRIZES ENTRADA
            System.out.println("MATRIZ CRITERIOS");
            printMat(mCrit);
            for (int j = 0; j < N_CRITERIOS; j++) {
                System.out.println("MATRIZ ALTERNATIVA " + (j + 1));
                printMat(mAlt[j]);
            }

            fOut.format("MATRIZES COMPARAÇAO:");
            fOut.format("%n");
            fOut.format("MATRIZ CRITERIOS");
            impressaoMatriz(mCrit, fOut);
            fOut.format("%n");
            for (int j = 0; j < N_CRITERIOS; j++) {
                fOut.format("MATRIZ ALTERNATIVA " + (j + 1));

                impressaoMatriz(mAlt[j], fOut);
                fOut.format("%n");
            }
            if (N_CRITERIOS < 3) {
                System.out.println("IMPOSSIVEL CONTINUAR: numero de criterios insuficiente.");
            } else {
                //NORMALIZAÇAO DAS MATRIZES
                for (int j = 0; j < N_CRITERIOS; j++) {
                    normalizarMatriz(mAlt[j], N_ALTERNATIVAS, N_ALTERNATIVAS, mAltNorm[j]);
                }

                normalizarMatriz(mCrit, N_CRITERIOS, N_CRITERIOS, mCritNorm);
                //PRINT E OUTPUT MATRIZES NORMALIZADAS
                fOut.format("%n");
                fOut.format("MATRIZES NORMAIS:");
                fOut.format("%n");
                fOut.format("MATRIZ CRITERIOS:");
                impressaoMatriz(mCritNorm, fOut);
                for (int j = 0; j < N_CRITERIOS; j++) {

                    fOut.format("%n");
                    fOut.format("MATRIZ " + (j + 1));

                    impressaoMatriz(mAltNorm[j], fOut);

                }
                System.out.println("CALCULOS UTILIZANDO VALORES APROXIMADOS: PRIMA 1"
                        + "\nCALCULOS UTILIZANDO VALORES EXATOS: PRIMA 2");
                String escolha = ler.nextLine();
                while (!"1".equals(escolha) && !"2".equals(escolha)) {

                    System.out.println("INTRODUZA VALORES VALIDOS:");
                    escolha = ler.nextLine();
                }

                switch (escolha) {

                    case "1":
                        System.out.println("APROXIMADO:");

                        criarVetPropAprox(mCritNorm, vetPCrit);

                        for (int j = 0; j < N_CRITERIOS; j++) {
                            criarVetPropAprox(mAltNorm[j], vetPAlt[j]);
                        }
                        fOut.format("%n");
                        fOut.format("VETOR PROPRIO CIRTERIOS:");

                        impressaoVet(vetPCrit, N_CRITERIOS, fOut);
                        fOut.format("%n");
                        for (int j = 0; j < N_CRITERIOS; j++) {
                            fOut.format("VETOR PROPRIO ALTERNATIVA " + (j + 1));

                            impressaoVet(vetPAlt[j], N_ALTERNATIVAS, fOut);
                            fOut.format("%n");
                        }

                        fOut.format("%n");
                        fOut.format("CONSISTENCIA:");
                        fOut.format("%n");
                        fOut.format("MATRIZ CRITERIOS:");
                        fOut.format("%n");
                        rc = consistencia(N_CRITERIOS, mCrit, vetPCrit, fOut);
                        if (limRC < rc) {
                            System.out.println("ATENÇAO: esta a trabalhar com criterios inconsistentes");
                        }
                        for (int i = 0; i < N_CRITERIOS; i++) {
                            fOut.format("%n");
                            fOut.format("MATRIZ ALTERNATIVA " + (i + 1) + ":");
                            fOut.format("%n");
                            rc = consistencia(N_ALTERNATIVAS, mAlt[i], vetPAlt[i], fOut);
                            if (limRC < rc) {
                                System.out.println("ATENÇAO: esta a trabalhar com valores inconsistentes no vetor alternativa " + (i + 1));
                            }
                        }

                        transpor(vetPAlt, vetPAltTrans, N_CRITERIOS, N_ALTERNATIVAS);

                        alternativa = melhorAlternativa(vetPAltTrans, N_ALTERNATIVAS, N_CRITERIOS, vetPCrit, vetPComposta, alt);
                        System.out.println("VETOR PRIORIDADE COMPOSTA");
                        for (int j = 0; j < N_ALTERNATIVAS; j++) {
                            System.out.println(vetPComposta[j]);
                        }
                        fOut.format("%n");
                        fOut.format("VETOR PRIORIDADE COMPOSTA");
                        impressaoVet(vetPComposta, N_ALTERNATIVAS, fOut);
                        fOut.format("%n");
                        fOut.format("%n");
                        fOut.format("A ALTERNATIVA E: " + alternativa);
                        System.out.println("A ALTERNATIVA E:" + alternativa);
                        break;

                    case "2":
                        System.out.println("EXATO:");

                        vetPCrit = calculoValorProprioEVetorProprioExato(mCrit, N_CRITERIOS);

                        for (int j = 0; j < N_CRITERIOS; j++) {
                            vetPAlt[j] = calculoValorProprioEVetorProprioExato(mAlt[j], N_ALTERNATIVAS);
                        }
                        fOut.format("%n");
                        fOut.format("VETOR PROPRIO CIRTERIOS:");

                        impressaoVet(vetPCrit, N_CRITERIOS, fOut);
                        fOut.format("%n");
                        for (int j = 0; j < N_CRITERIOS; j++) {

                            fOut.format("VETOR PROPRIO ALTERNATIVA " + (j + 1));

                            impressaoVet(vetPAlt[j], N_ALTERNATIVAS, fOut);
                            fOut.format("%n");
                        }
                        fOut.format("%n");
                        fOut.format("%n");
                        fOut.format("CONSISTENCIA:");
                        fOut.format("%n");
                        fOut.format("MATRIZ CRITERIOS:");
                        rc = consistencia(N_CRITERIOS, mCrit, vetPCrit, fOut);
                        if (limRC < rc) {
                            System.out.println("ATENÇAO: esta a trabalhar com criterios inconsistentes");
                        }
                        for (int i = 0; i < N_CRITERIOS; i++) {
                            rc = consistencia(N_ALTERNATIVAS, mAlt[i], vetPAlt[i], fOut);
                            if (limRC < rc) {
                                System.out.println("ATENÇAO: esta a trabalhar com valores inconsistentes no vetor alternativa " + (i + 1));
                            }
                        }

                        transpor(vetPAlt, vetPAltTrans, N_CRITERIOS, N_ALTERNATIVAS);

                        alternativa = melhorAlternativa(vetPAltTrans, N_ALTERNATIVAS, N_CRITERIOS, vetPCrit, vetPComposta, alt);
                        System.out.println("VETOR PRIORIDADE COMPOSTA");
                        for (int j = 0; j < N_ALTERNATIVAS; j++) {
                            System.out.println(vetPComposta[j]);
                        }
                        fOut.format("%n");
                        fOut.format("VETOR PRIORIDADE COMPOSTA");
                        impressaoVet(vetPComposta, N_ALTERNATIVAS, fOut);
                        fOut.format("%n");
                        fOut.format("%n");
                        fOut.format("A ALTERNATIVA E: " + alternativa);
                        System.out.println("A ALTERNATIVA: " + alternativa);

                        for (int j = 0; j < N_ALTERNATIVAS; j++) {

                        }
                        break;

                }
            }
        }
        System.out.println("!!ATENÇAO: NAO SE ESQUEÇA SE VERIFICAR O FICHEIRO DE ERROS((FicheiroErros.txt))!!");
        fOut.close();
        erro.close();
    }

}
